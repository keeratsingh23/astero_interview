# Astero_Interview
Investment Calculator v1

Run the project as a Java Application and the program begins.

Once the application is launched, enter the required information to calculate investment returns.
After clicking 'Get Results', a graph of the investment over time will be displayed as well as the 
total real and nominal return.

Required inputs are initial investment, annual rate, and years to be invested.

This program was written using javafx. Please rever to the javadocs in the doc folder for information
about classes and methods.


