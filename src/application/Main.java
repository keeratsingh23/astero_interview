package application;
	
import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;

/**
 *  This class is used for starting the program and creating the main window.
 * 
 * @author keeratsingh
 *
 */

public class Main extends Application {
	
	Stage mainWindow;
	Stage alertWindow;
	
	
	public static void main(String[] args) {
		launch(args);
	}
	
	/**
	 *  This function starts the program.
	 */

	public void start(Stage primaryStage) throws Exception {
		
		//Launch the main window
		mainWindow = new Stage();
		
		Parent root = FXMLLoader.load(getClass().getResource("MainWindow.fxml"));
		Scene scene = new Scene(root, 800, 800);

		mainWindow.setScene(scene);
		mainWindow.setTitle("Singh Bank Investment Returns");
		mainWindow.resizableProperty().setValue(Boolean.FALSE);
		mainWindow.show();
		
		//If the user tries to exit from program, call closeWindow method
		mainWindow.setOnCloseRequest(e -> {
			e.consume();
			try {
				closeWindow();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
		
	}

	/** This function allows the user to make sure they want to exit the program before
	 *  it actually closes.
	 * 
	 * @throws Exception
	 */
	public void closeWindow() throws Exception {
		
		//Launch the alert window method
		Parent root = FXMLLoader.load(getClass().getResource("AlertWindow.fxml"));
		alertWindow = new Stage();
		alertWindow.setScene(new Scene(root, 300, 200));
		alertWindow.setTitle("Alert Box");
		alertWindow.resizableProperty().setValue(Boolean.FALSE);
		alertWindow.initModality(Modality.APPLICATION_MODAL);
		alertWindow.showAndWait();
		
	}
}
