package application;

import java.io.IOException;
import java.text.DecimalFormat;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * This class is the controller for the main window
 * 
 * @author keeratsingh
 *
 */
public class MainWindowController {
	
	/**
	 * Data members of the class
	 */
	double initialAmount;
	double annualReturn;
	double taxRate;
	double inflationRate;
	double bankFee;
	int years;
	boolean percentOfInterest = false;
	double[] amountEachYear;
	double[] amountEachYearWOInflation;
	double[] principalInflation;
	boolean helpWindowOpen = false;	//True when user has the help window open
	boolean errorWindowOpen = false;
	Stage helpWindow;
	Stage graph;
	
	/**
	 * Items included in the interface
	 */
	@FXML
	Button submitButton;
	
	@FXML
	TextField initialText;
	
	@FXML
	TextField annualReturnText;
	
	@FXML
	TextField taxRateText;
	
	@FXML
	TextField inflationRateText;
	
	@FXML
	TextField bankFeeText;
	
	@FXML
	TextField yearsText;
	
	@FXML
	MenuItem helpButton;
	
	@FXML
	MenuItem closeButton;
	
	@FXML
	RadioButton boxOne;
	
	@FXML
	RadioButton boxTwo;
	
	@FXML
	AreaChart<String, Number> inflationChart;
	
	@FXML
	AreaChart<String, Number> noInflationChart;
	
	@FXML
	CategoryAxis c1;
	
	@FXML
	NumberAxis n1;
	
	@FXML
	CategoryAxis c2;
	
	@FXML
	NumberAxis n2;
	
	@FXML
	Label totalRealInterest;
	
	@FXML
	Label totalNominalInterest;
	
	@FXML
	Label initialWarning;
	
	@FXML
	Label taxWarning;
	
	@FXML
	Label bankWarning;
	
	@FXML
	Label yearsWarning;
	
	@FXML
	Label annualWarning;
	
	@FXML
	Label inflationWarning;

	/**
	 * Handles the event that the submit button is pressed.
	 * 
	 * @throws Exception
	 */
	public void submitButtonPressed() throws Exception {
		
		try {
			//Check to make sure correct data types entered
			//and store the values in corresponding variables
			
			try {
				//In case user entered number with commas
				String[] iAmountArray = initialText.getText().split(",");
				String iAmount = "";
				for(String i : iAmountArray) {
					iAmount += i;
				}
				
				//Attempt to parse the initial amount text as a double
				initialAmount = Double.parseDouble(iAmount);
				
				//Convert to string with commas for user readability
				DecimalFormat df = new DecimalFormat("#,###,##0.00");
				String updatedText = df.format(initialAmount);
				initialText.setText(updatedText);
			}catch(NumberFormatException e) {
				throw new NumberFormatException();
			}
			
			//Attempt to parse the annual return percentage text as a double
			try {
			annualReturn = Double.parseDouble(annualReturnText.getText())/100;
			
			}catch(NumberFormatException e) {
				if(annualReturnText.getText().contentEquals("")) {
					annualReturn = 0;
				}
				else {
					throw new NumberFormatException();
				}
			}
			
			//Attempt to parse the tax rate percentage text as a double
			try {
				taxRate = Double.parseDouble(taxRateText.getText())/100;
			}catch(NumberFormatException e) {
				if(taxRateText.getText().contentEquals("")) {
					taxRate = 0;
				}
				else {
					throw new NumberFormatException();
				}
			}
			
			//Attempt to parse the inflation rate percentage text as a double
			try {
			inflationRate = Double.parseDouble(inflationRateText.getText())/100;
			}catch(NumberFormatException e) {
				if(inflationRateText.getText().contentEquals("")) {
					inflationRate = 0;
				}
				else {
					throw new NumberFormatException();
				}
			}
			
			//Attempt to parse the bank fee percentage text as a double
			try {
				bankFee = Double.parseDouble(bankFeeText.getText())/100;
			}catch(NumberFormatException e) {
				if(bankFeeText.getText().contentEquals("")) {
					bankFee = 0;
				}
				else {
					throw new NumberFormatException();
				}
			}
			
			//Attempt to parse the year text as an integer
			try {
				years = Integer.parseInt(yearsText.getText());
			}catch(NumberFormatException e) {
				if(yearsText.getText().contentEquals("")) {
					years = 0;
				}
				else {
					throw new NumberFormatException();
				}
			}
			
			//If any value less than 0, throw exception
			if(initialAmount < 0 || annualReturn < 0 || taxRate < 0 || inflationRate < 0 || bankFee < 0 || years < 0) {
				throw new NumberFormatException("All values must be postive.");
			}
			
			//If these values not greater than 0 throw exception
			if(!(initialAmount > 0) || !(annualReturn > 0) || !(years > 0)) {
				throw new NumberFormatException("Values cannot be zero");
			}
			
			launchGraph();
			
		//Displays error box if incorrect data types entered	
		}catch(NumberFormatException e) {
			
			displayError();
			
		}
		
	}
	
	/**
	 * Handles the event that the help button is pressed
	 * 
	 * @throws Exception
	 */
	public void helpButtonPressed() throws Exception {
			
		//Creates and displays the help window
		//Sets helpWindowOpen to true
		helpWindow = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("HelpWindow.fxml"));
		helpWindow.setScene(new Scene(root, 600, 400));
		helpWindow.setTitle("Help");
		helpWindow.initModality(Modality.APPLICATION_MODAL);
		helpWindow.resizableProperty().setValue(Boolean.FALSE);
		helpWindow.showAndWait();
	}
	
	/**
	 * Handles the event that the first radio button is selected
	 */
	public void boxOneSelected() {
		//Only one check box can be selected at a time 
		if(boxTwo.isSelected()) {
			boxTwo.setSelected(false);
		}
		//Do not allow radio button to be unselected
		boxOne.setSelected(true);
		percentOfInterest = true;
	}
	
	/**
	 * Handles the event that the second radio button is selected
	 */
	public void boxTwoSelected() {
		//Only one check box can be selected at a time 
		if(boxOne.isSelected()) {
			boxOne.setSelected(false);
		}
		//Do not allow radio button to be unselected
		boxTwo.setSelected(true);
		percentOfInterest = false;
	}

	/**
	 * This method creates the graph of investment returns
	 * 
	 * @see application.InvestmentCalculator
	 * @throws Exception
	 */
	public void launchGraph() throws Exception {
		
		//Create new investment calculator object to calculate investment return
		//using the user provided input
		InvestmentCalculator x = new InvestmentCalculator(initialAmount,annualReturn, taxRate, inflationRate, bankFee, percentOfInterest, years);
		
		//Call the function that calculates the investment return
		//and store them in the appropriate arrays
		x.calculateReturn();
		amountEachYear = x.getAmountEachYear();
		amountEachYearWOInflation = x.getAmountEachYearWOInflation();
		
		//Call the function that calculates principal inflation
		//and store it into the array
		x.calculatePrincipalInflation();
		principalInflation = x.getPrincipalInflation();
		
		//Set the title of axis for graph
		c1.setLabel("Time (Years)");
		n1.setLabel("Value ($)");
		
		//Clear contents of graph before re-graphing
		inflationChart.getData().clear();
		
		//Create graph for principal investment
		XYChart.Series<String, Number> series3 = new XYChart.Series<String, Number>();
		series3.getData().add(new XYChart.Data<String, Number>("0", initialAmount));
		series3.setName("Principal");
		for(int i = 0; i < years; i++) {
			series3.getData().add(new XYChart.Data<String, Number>(String.valueOf(i+1), principalInflation[i]));
		}
						
		inflationChart.getData().add(series3);
		
		//Create graph for return not including inflation
		XYChart.Series<String, Number> series2 = new XYChart.Series<String, Number>();
		series2.getData().clear();
		series2.getData().add(new XYChart.Data<String, Number>("0", initialAmount));
		series2.setName("Nominal");
		for(int i = 0; i < amountEachYearWOInflation.length; i++) {
			series2.getData().add(new XYChart.Data<String, Number>(String.valueOf(i+1), amountEachYearWOInflation[i]));
		}
				
		inflationChart.getData().add(series2);
		
		//Only plots if user has entered an inflation rate
		if(inflationRate > 0) {
			//Create graph for return including inflation
			XYChart.Series<String, Number> series1 = new XYChart.Series<String, Number>();
			series1.getData().clear();
			series1.getData().add(new XYChart.Data<String, Number>("0", initialAmount));
			series1.setName("Real");
			for(int i = 0; i < amountEachYear.length; i++) {
				series1.getData().add(new XYChart.Data<String, Number>(String.valueOf(i+1), amountEachYear[i]));
			}
			
			inflationChart.getData().add(series1);
			
		}
	
		//Output the amount of money earned
		DecimalFormat df = new DecimalFormat("#,###,##0.00");
		String totalRI = df.format(amountEachYear[years-1] - initialAmount);
		String totalNI = df.format(amountEachYearWOInflation[years-1] - initialAmount);
		
		totalRealInterest.setText("$ " + totalRI.substring(0, totalRI.indexOf(".") + 3));
		totalNominalInterest.setText("$ " + totalNI.substring(0, totalNI.indexOf(".") + 3));

	}
	
	/**
	 * Makes sure user wants to close
	 * 
	 * @throws Exception
	 */
	public void closeWindow() throws Exception {
		//Launch alert window
		Parent root = FXMLLoader.load(getClass().getResource("AlertWindow.fxml"));
		Stage alertWindow = new Stage();
		alertWindow.setScene(new Scene(root, 300, 200));
		alertWindow.setTitle("Alert Box");
		alertWindow.resizableProperty().setValue(Boolean.FALSE);
		alertWindow.initModality(Modality.APPLICATION_MODAL);
		alertWindow.showAndWait();
		
	}
	
	public void displayError() throws Exception{
		//Creates and displays the error window
		errorWindowOpen = true;
		Stage errorWindow = new Stage();
		errorWindow.initModality(Modality.APPLICATION_MODAL);	//User cannot interact with the program until error message closed
		Parent root = FXMLLoader.load(getClass().getResource("ErrorWindow.fxml"));
		errorWindow.setScene(new Scene(root, 600, 200));
		errorWindow.setTitle("Error Message");
		errorWindow.resizableProperty().setValue(Boolean.FALSE);
		errorWindow.showAndWait();
	}
	/**
	 * Called when user is typing in initial amount field
	 * 
	 */
	public void initialAmountChanged() {
		isValid(initialText.getText(), initialWarning);
	}
	
	/**
	 * Called when user is typing in initial amount field
	 * 
	 */
	public void yearsChanged() {
		isValid(yearsText.getText(), yearsWarning);
	}
	
	/**
	 * Called when user is typing in initial amount field
	 * 
	 */
	public void annualReturnChanged() {
		isValid(annualReturnText.getText(), annualWarning);
	}
	
	/**
	 * Called when user is typing in initial amount field
	 * 
	 */
	public void taxRateChanged() {
		isValid(taxRateText.getText(), taxWarning);
	}
	
	/**
	 * Called when user is typing in initial amount field
	 * 
	 */
	public void bankFeeChanged() {
		isValid(bankFeeText.getText(), bankWarning);
	}
	
	/**
	 * Called when user is typing in initial amount field
	 * 
	 */
	public void inflationChanged() {
		isValid(inflationRateText.getText(), inflationWarning);
	}
	
	/**
	 * 
	 * @param text
	 * @return validity of text entered
	 */
	public boolean isValid(String text, Label l) {
		String[] alphabet = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","!","@","#","$","%","^","&","*","(",")","-","_","+","=","{","}","[","]","|","\\",":",";","\"","'","<",",",">",".","/","?"};
		boolean valid = true;
		
		for(int i = 0; i < alphabet.length; i++) {
			if(text.toLowerCase().contains(alphabet[i])) {
				l.setText("Incorrect Value");
				valid = false;
			}
		}
		if(valid) {
			l.setText("");
		}
		return valid;
	}

}
