package application;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * This class is the controller for the alert window
 * 
 * @author keeratsingh
 *
 */
public class AlertWindowController {
	
	@FXML
	Button yesButton;
	@FXML
	Button noButton;
	
	/**
	 * Handles the event that the yes button is pressed
	 */
	public void yesButtonPressed() {
		//Close the alert window
		Stage stage = (Stage) yesButton.getScene().getWindow();
		stage.close();
		//Close program
		Platform.exit();
	}
	
	/**
	 * Handles the event that the no button is pressed
	 */
	public void noButtonPressed() {
		//Close the alert window
		Stage stage = (Stage) noButton.getScene().getWindow();
		stage.close();
	}

}
