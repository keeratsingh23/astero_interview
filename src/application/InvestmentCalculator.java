package application;

/**
 * This class calculates the investment return based on given inputs.
 * The inputs needed are the initial amount of money invested, the annual return rate,
 * the tax rate, the inflation rate, the bank fee percentage, boolean value determining 
 * the method of the bank fee, and the number of years the money is to be invested for.
 * 
 * @author keeratsingh
 *
 */
public class InvestmentCalculator {
	
	private double initialAmount;
	//rate of return
	private double annualReturn;
	private double taxRate;
	private double inflationRate;
	private double bankFee;
	private boolean percentOfInterest;
	//Current amount with inflation
	private double currentAmount = 0;
	//Current amount without inflation
	private double currentAmountWOInflation = 0;
	//Each year with inflation
	private double[] amountEachYear;
	//Each year without inflation
	private double[] amountEachYearWOInflation;
	//Initial investment adjusted with inflation
	private double[] principalInflation;
	private int years;
	
	/**
	 * Default constructor
	 */
	public InvestmentCalculator() {
		
		initialAmount = 1000;
		annualReturn = 0.05;
		taxRate = 0.15;
		inflationRate = 0.03;
		bankFee = 0.01;
		percentOfInterest = false;
		years = 15;
		amountEachYear = new double[years];
		amountEachYearWOInflation = new double[years];
		principalInflation = new double[years];
		
	}
	
	/**
	 * Overloaded constructor
	 * 
	 * @param initialAmount the initial amount
	 * @param annualReturn the annual return rate
	 * @param taxRate the tax rate
	 * @param inflationRate the inflation rate
	 * @param bankFee the bank fee percentage
	 * @param percentOfInterest boolean value determining bank fee method
	 * @param years number of years the be invested
	 */
	public InvestmentCalculator(double initialAmount, double annualReturn, double taxRate, double inflationRate, double bankFee, boolean percentOfInterest, int years) {
		
		this.initialAmount = initialAmount;
		this.annualReturn = annualReturn;
		this.taxRate = taxRate;
		this.inflationRate = inflationRate;
		this.bankFee = bankFee;
		this.percentOfInterest = percentOfInterest;
		this.years = years;
		amountEachYear = new double[years];
		amountEachYearWOInflation = new double[years];
		principalInflation = new double[years];
		
	}
	
	/**
	 * @return the initialAmount
	 */
	public double getInitialAmount() {
		return initialAmount;
	}

	/**
	 * @param initialAmount the initialAmount to set
	 */
	public void setInitialAmount(double initialAmount) {
		this.initialAmount = initialAmount;
	}

	/**
	 * @return the annualReturn
	 */
	public double getAnnualReturn() {
		return annualReturn;
	}

	/**
	 * @param annualReturn the annualReturn to set
	 */
	public void setAnnualReturn(double annualReturn) {
		this.annualReturn = annualReturn;
	}

	/**
	 * @return the taxRate
	 */
	public double getTaxRate() {
		return taxRate;
	}

	/**
	 * @param taxRate the taxRate to set
	 */
	public void setTaxRate(double taxRate) {
		this.taxRate = taxRate;
	}

	/**
	 * @return the inflationRate
	 */
	public double getInflationRate() {
		return inflationRate;
	}

	/**
	 * @param inflationRate the inflationRate to set
	 */
	public void setInflationRate(double inflationRate) {
		this.inflationRate = inflationRate;
	}

	/**
	 * @return the bankFee
	 */
	public double getBankFee() {
		return bankFee;
	}

	/**
	 * @param bankFee the bankFee to set
	 */
	public void setBankFee(double bankFee) {
		this.bankFee = bankFee;
	}

	/**
	 * @return the percentOfInterest
	 */
	public boolean isPercentOfInterest() {
		return percentOfInterest;
	}

	/**
	 * @param percentOfInterest the percentOfInterest to set
	 */
	public void setPercentOfInterest(boolean percentOfInterest) {
		this.percentOfInterest = percentOfInterest;
	}

	/**
	 * @return the currentAmount
	 */
	public double getCurrentAmount() {
		return currentAmount;
	}

	/**
	 * @param currentAmount the currentAmount to set
	 */
	public void setCurrentAmount(double currentAmount) {
		this.currentAmount = currentAmount;
	}

	/**
	 * @return the years
	 */
	public int getYears() {
		return years;
	}

	/**
	 * @param years the years to set
	 */
	public void setYears(int years) {
		this.years = years;
	}
	
	/**
	 * @return the currentAmountWOInflation
	 */
	public double getCurrentAmountWOInflation() {
		return currentAmountWOInflation;
	}

	/**
	 * @param currentAmountWOInflation the currentAmountWOInflation to set
	 */
	public void setCurrentAmountWOInflation(double currentAmountWOInflation) {
		this.currentAmountWOInflation = currentAmountWOInflation;
	}
	
	/**
	 * @return the amountEachYear
	 */
	public double[] getAmountEachYear() {
		return amountEachYear;
	}

	/**
	 * @param amountEachYear the amountEachYear to set
	 */
	public void setAmountEachYear(double[] amountEachYear) {
		this.amountEachYear = amountEachYear;
	}

	/**
	 * @return the amountEachYearWOInflation
	 */
	public double[] getAmountEachYearWOInflation() {
		return amountEachYearWOInflation;
	}

	/**
	 * @param amountEachYearWOInflation the amountEachYearWOInflation to set
	 */
	public void setAmountEachYearWOInflation(double[] amountEachYearWOInflation) {
		this.amountEachYearWOInflation = amountEachYearWOInflation;
	}
	
	/**
	 * @return the principalInflation
	 */
	public double[] getPrincipalInflation() {
		return principalInflation;
	}

	/**
	 * @param principalInflation the principalInflation to set
	 */
	public void setPrincipalInflation(double[] principalInflation) {
		this.principalInflation = principalInflation;
	}

	/**
	 * Calculates the return for all years
	 */
	public void calculateReturn() {
		
		//Set the current amount of money to the initial amount
		currentAmount = initialAmount;
		currentAmountWOInflation = initialAmount;
		
		//Loop through the array holding the return after each year with inflation
		for(int i = 0; i < amountEachYear.length; i++) {
			
			//Set calculated amount to corresponding year
			updateCurrentAmount();
			amountEachYear[i] = currentAmount;

		}
		
		//Loop through the array holding the return after each year without inflation
		for(int i = 0; i < amountEachYearWOInflation.length; i++) {
					
			//Set calculated amount to corresponding year
			updateCurrentAmountWOInflation();
			amountEachYearWOInflation[i] = currentAmountWOInflation;
					
		}
		
	}
	/**
	 * 
	 * @return rate with inflation
	 */
	//Returns the rate of return with inflation
	public double getRealRate() {
		
		double rate = (1 + (annualReturn * (1 - taxRate)))/(1 + inflationRate) - 1;
		return rate;
		
	}
	
	/**
	 * 
	 * @return rate without inflation
	 */
	//Returns the rate of return without inflation
	public double getRateWOInflation() {
		
		
		double rate = (1 + (annualReturn * (1 - taxRate))) - 1;
		return rate;
		
	}
	
	/**
	 * Updates the current amount with inflation
	 */
	public void updateCurrentAmount() {
		
		double investedAmount = currentAmount;
		currentAmount = currentAmount + (currentAmount * getRealRate());
		//If user selected to give fees based on interest
		if(percentOfInterest) {
			currentAmount = currentAmount - (currentAmount * getRealRate() * bankFee);
		}
		//If user selected to give fees based on invested amount
		else {
			currentAmount = currentAmount - (investedAmount * bankFee);
		}
		
	}
	
	/**
	 * Updates the current amount without inflation
	 */
	public void updateCurrentAmountWOInflation() {
		
		double investedAmount = currentAmountWOInflation;
		currentAmountWOInflation = currentAmountWOInflation + (currentAmountWOInflation * getRateWOInflation());
		//If user selected to give fees based on interest
		if(percentOfInterest) {
			currentAmountWOInflation = currentAmountWOInflation - (currentAmountWOInflation * getRateWOInflation() * bankFee);
		}
		//If user selected to give fees based on invested amount
		else {
			currentAmountWOInflation = currentAmountWOInflation - (investedAmount * bankFee);
		}
		
	}
	
	/**
	 * Calculates the decline in principal amount
	 */
	public void calculatePrincipalInflation() {
		
		double principalAmount = initialAmount;
		
		//Loop through and deduct the principal amount due to inflation each year
		for(int i = 0; i < principalInflation.length; i++) {
			principalAmount = principalAmount - (principalAmount * inflationRate);
			principalInflation[i] = principalAmount;
		}
		
	}

}
