package application;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * This class is the controller for the error window
 * 
 * @author keeratsingh
 *
 */
public class ErrorWindowController {
	
	@FXML
	Button okButton;
	
	/**
	 * Handles the event that the 'ok' button is pressed
	 */
	public void okButtonPressed() {
		//Simply close the window
		Stage stage = (Stage) okButton.getScene().getWindow();
		stage.close();
	}
}
